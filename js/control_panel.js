var btnRegistrar2 = document.getElementById("registrard");
var xy;
var users = localStorage.getItem("users");
users = JSON.parse(users);
if (users === null)
    users = [];


var guser = JSON.parse(localStorage.getItem("guser"));
var productos = JSON.parse(localStorage.getItem("inventario"));
var btnVerUsers = document.getElementById("users");
var btnVerInv = document.getElementById("inv");
var btnClear = document.getElementById("clearT");
var tablediv = document.getElementById("tablainfo");

var nombreProd = document.getElementById("nombreProduct");
var descProd = document.getElementById("descProduct");
var imgProd = document.getElementById("imgProduct");
var imgE = document.getElementById("bannerImg");
var priceProd = document.getElementById("priceProduct");
var btnRegistrar = document.getElementById("registrar");
var btnBack = document.getElementById("back");
btnBack.addEventListener("click", function () {
    window.location = "../index.html";
})

imgProd.addEventListener("change", function () {
    input = this;

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            imgE.src = e.target.result;
        }
        imgE.style.height = "auto";
        reader.readAsDataURL(input.files[0]);
    }
})

btnRegistrar.addEventListener("click", function () {
    //console.log(priceProd.value);
    if (isNaN(priceProd.value) || priceProd.value < 0 || priceProd.value == "") {
        alert("Precio inválido");
    } else {
        try {
            var newproducto = {
                id: productos[productos.length - 1].id + 1,
                nombre: nombreProd.value,
                desc: descProd.value,
                img: "img/" + imgProd.files[0].name,
                precio: parseFloat(priceProd.value)
            }
            //console.log(newproducto);
            productos.push(newproducto);
            localStorage.setItem("inventario", JSON.stringify(productos));
            alert("Producto agregado correctamente");
            location.reload();
        } catch (error) {
            //alert("Ha ocurrido un error")
        }
    }
})





function removeToLocalStorage(dataId)
    {
            todoList = [];
            todoList = JSON.parse(localStorage.getItem("users"));
            todoList.splice(dataId, 1);
            localStorage.setItem("users", JSON.stringify(todoList));
            verUsuarios();
    }

function editItem(dataId){
        console.log(dataId);
        var user = document.getElementById("user");
        var nombre = document.getElementById("nombre");
        var apellido = document.getElementById("apellido");
        var email = document.getElementById("email");
        var direccion = document.getElementById("direccion");
        var codigopostal = document.getElementById("codigopostal");
        var pais = document.getElementById("pais");
        var telefono = document.getElementById("telefono");
        var tarjeta = document.getElementById("tarjeta");
        var pass = document.getElementById("passs");

        var todoList = [];
        todoList = JSON.parse(localStorage.getItem("users"));
    
        user.value = todoList[dataId].username;
        nombre.value = todoList[dataId].nombre;
        apellido.value = todoList[dataId].apellido;
        email.value = todoList[dataId].correo;
        direccion.value = todoList[dataId].direccion;
        codigopostal.value = todoList[dataId].cpostal;
        telefono.value = todoList[dataId].telefono;
        tarjeta.value = todoList[dataId].tarjeta;
        pass.value = todoList[dataId].pass;
        document.getElementById("registrard").setAttribute("id",dataId);
        document.getElementById(dataId).setAttribute("name","update");
        document.getElementById(dataId).innerText = "Guardar";
        xy = document.getElementsByTagName("button")[0].getAttribute("id"); 
    }


btnVerUsers.addEventListener("click", function () {
    verUsuarios();
    document.getElementById("formProducto").style.display = "none"; 
})

function verUsuarios(){
    tablediv.innerHTML = "";
    var tbl = document.createElement('table');
    tbl.className = "tb";
    var tbdy = document.createElement('tbody');
    var frow = ["Rol", "User", "Pass", "Carrito", "Nombres", "Apellidos", "Email", "País", "Postal", "Dirección", "Tarjeta", "Tel","Eliminar","Modificar"];
    var tr = document.createElement('tr');
    tr.className = "head";
    for (var j = 0; j < 14; j++) {
        var td = document.createElement('td');
        td.innerHTML = frow[j];
        tr.appendChild(td);
    }
    tbdy.appendChild(tr);
    for (var i = 0; i < users.length; i++) {
        var tr = document.createElement('tr');
        for (const prop in users[i]) {
            var td = document.createElement('td');
            td.innerHTML = `${users[i][prop]}`;
            tr.appendChild(td);
        }
            var td = document.createElement('td');
            var td1 = document.createElement('td');
            td.innerHTML = "<a class='button' onclick='" + "removeToLocalStorage(" + i + ")" + "'>Borrar</a>";
            td1.innerHTML = "<a class='button' onclick='" + "editItem(" + i + ")" + "' >Modificar</a>";
            tr.appendChild(td);
            tr.appendChild(td1);
        tbdy.appendChild(tr);
    }
    tbl.appendChild(tbdy);
    tablediv.appendChild(tbl)
}



btnVerInv.addEventListener("click", verProductos);

function eliminarProd(index) {
    for (i = 0; i < productos.length; i++) {
        if (index == productos[i].id) {
            productos.splice(i, 1);
        }
    }
    localStorage.setItem("inventario", JSON.stringify(productos));
    verProductos();
}

btnClear.addEventListener("click", function () {
    tablediv.innerHTML = "";
    document.getElementById("formProducto").style.display = "block"; 
})

function verProductos() {
    tablediv.innerHTML = "";
    var tbl = document.createElement('table');
    tbl.className = "tb";
    tbl.setAttribute('border', '1');
    var tbdy = document.createElement('tbody');
    var frow = ["ID", "Nombre", "Descripción", "Img", "Precio", "Borrar"];
    var tr = document.createElement('tr');
    tr.className = "head";
    for (var j = 0; j < frow.length; j++) {
        var td = document.createElement('td');
        td.innerHTML = frow[j];
        tr.appendChild(td);
    }
    tbdy.appendChild(tr);
    for (var i = 0; i < productos.length; i++) {
        var tr = document.createElement('tr');
        for (const prop in productos[i]) {
            var td = document.createElement('td');
            td.innerHTML = `${productos[i][prop]}`;
            tr.appendChild(td)
        }
        var td = document.createElement('td');
        td.className = "btn";
        td.innerHTML = "<a class='button' onclick='" + "eliminarProd(" + productos[i].id + ")" + "'>Borrar</a>";
        tr.appendChild(td);
        tbdy.appendChild(tr);
    }
    tbl.appendChild(tbdy);
    tablediv.appendChild(tbl)
}



function AgregarUsuario() {
    var rol = document.getElementById("Rol").value;
    var user = document.getElementById("user").value;
    var nombre = document.getElementById("nombre").value;
    var apellido = document.getElementById("apellido").value;
    var email = document.getElementById("email").value;
    var direccion = document.getElementById("direccion").value;
    var codigopostal = document.getElementById("codigopostal").value;
    var pais = document.getElementById("pais").value;
    var telefono = document.getElementById("telefono").value;
    var tarjeta = document.getElementById("tarjeta").value;
    var pass = document.getElementById("passs").value;
    var carrito = [];
    todoList = [];
    todoList.push({
        rol: rol,
        username: user,
        nombre: nombre,
        apellido: apellido,
        correo: email,
        direccion: direccion,
        cpostal: codigopostal,
        pais: pais,
        telefono: telefono,
        tarjeta: tarjeta,
        pass: pass,
        carrito: carrito,
    });
    var todoListCopia = todoList.slice();
    users.splice(xy, 1, todoListCopia[0]);
    localStorage.setItem("users", JSON.stringify(users));
}


var datosCorrectos=true;
function validacion(){
    var rol = document.getElementById("Rol").value;
    var user = document.getElementById("user").value;
    var nombre = document.getElementById("nombre").value;
    var apellido = document.getElementById("apellido").value;
    var email = document.getElementById("email").value;
    var direccion = document.getElementById("direccion").value;
    var codigopostal = document.getElementById("codigopostal").value;
    var pais = document.getElementById("pais").value;
    var telefono = document.getElementById("telefono").value;
    var tarjeta = document.getElementById("tarjeta").value;
    var pass = document.getElementById("passs").value;
    var cpass = document.getElementById("cpasss").value;

    var Vuser = new RegExp('(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,15})$','g');//6 caracteres a 15 incluyendo 1 numero ejemplo edward5
    var Vtodo = new RegExp('^[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])+))*$','g');//Todo tipo de texto sin incluir numeros y simbolos
    var Vtodo1 = new RegExp('^[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])+))*$','g');//Todo tipo de texto sin incluir numeros y simbolos
    var Vtodo2 = new RegExp('^[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])+))*$','g');//Todo tipo de texto sin incluir numeros y simbolos
    var Vemail = new RegExp('^[a-z]+[@]{1}[a-z]+[\.]{1}[a-z]{2,3}$','g');//somethingxd@algo.es
    var Vpostal = new RegExp('^[0-9]+$','g');//254
    var VTelefono = new RegExp('^[0-9]{4}[\-]{1}[0-9]{4}$','g');//7895-2562
    var VTarjeta = new RegExp('^4[1-9][0-9]{14}$','g');// Ejemplo 4154968792548316
    var Vcontra = new RegExp('(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,15})$','g');// Ejemplo 11223a     al menos un numero y 6 caracteres

    if(Vuser.test(user)){
        document.getElementById("user").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("user").className = 'error';
        return false;
    }

    if(Vtodo.test(nombre)){
        document.getElementById("nombre").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("nombre").className = 'error';
        return false;
    }

    if(Vtodo1.test(apellido)){
        document.getElementById("apellido").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("apellido").className = 'error';
        return false;
    }

    if(Vemail.test(email)){
        
        document.getElementById("email").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("email").className = 'error';
        return false;
    }

    if(Vtodo2.test(direccion)){
        
        document.getElementById("direccion").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("direccion").className = 'error';
        return false;
    }

    
    if(Vpostal.test(codigopostal)){
        document.getElementById("codigopostal").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("codigopostal").className = 'error';
        return false;
    }

    if(VTelefono.test(telefono)){
        document.getElementById("telefono").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("telefono").className = 'error';
        return false;
    }

    if(VTarjeta.test(tarjeta)){
        document.getElementById("tarjeta").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("tarjeta").className = 'error';
        return false;
    }

    if(Vcontra.test(pass)){
        document.getElementById("passs").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("passs").className = 'error';
        return false;
    }

    if(cpass.match(pass)){
        document.getElementById("cpasss").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("cpasss").className = 'error';
        return false;
    }
    
    datosCorrectos = true;

}

btnRegistrar2.addEventListener("click", function () {
    if (datosCorrectos === true){
        AgregarUsuario();
    } 
})
