var focusprod = localStorage.getItem("focusprod");
var users = localStorage.getItem("users");
var bdd = JSON.parse(localStorage.getItem("inventario"));
var guser = JSON.parse(localStorage.getItem("guser"));
var carrito = JSON.parse(localStorage.getItem("carro"));
var cantidad = JSON.parse(localStorage.getItem("cantidad"));

var imgprod = document.getElementById("imgprod");
var nombre = document.getElementById("nombre");
var desc = document.getElementById("desc");
var precio = document.getElementById("precio");
var btncomprar = document.getElementById("comprar");

let item = obtenerItem(parseInt(focusprod));

imgprod.src = "../" + item.img;
nombre.innerHTML = item.nombre;
desc.innerHTML = item.desc;
precio.innerHTML = "$" + item.precio.toFixed(2);
btncomprar.addEventListener("click",function(){
    let index = parseInt(focusprod, 10);
            if (carrito.indexOf(index) === -1) {
                alert('Se ha añadido al carrito');
                carrito.push(index);
                localStorage.setItem("carro", JSON.stringify(carrito))
                cantidad.push(1);
                localStorage.setItem("cantidad", JSON.stringify(cantidad));
            } else {
                alert('Ya se encuentra en el carrito');
            }
})

function obtenerItem(index){
    for(i=0;i<bdd.length; i++){
        if(index == bdd[i].id){
            return bdd[i];
        }
    }
}