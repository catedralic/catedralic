var db = JSON.parse(localStorage.getItem("inventario"));
var promo = JSON.parse(localStorage.getItem("promo"));
var cantidad = JSON.parse(localStorage.getItem("cantidad"));

var carrito = JSON.parse(localStorage.getItem("carro"));

if (localStorage.getItem("focusprod") === null) {
    localStorage.setItem("focusprod", 0);
    var focusprod = localStorage.getItem("focusprod");
} else {
    var focusprod = localStorage.getItem("focusprod");
}

var items = document.querySelector('#principal');

window.onload = renderItems;

var botones;

function renderItems() {

    for (let i in promo) {
        let index = promo[i];
        let item = obtenerItem(index);
        //estructura
        let card = document.createElement('div');
        card.classList.add('card');

        let imagen = document.createElement('img');
        imagen.src = "../"  + item.img;

        //estructura interna
        let div_inter = document.createElement('div');

        let titulo = document.createElement('h4');
        titulo.textContent = verifyString(item.nombre,32);

        let precio = document.createElement('h5');
        precio.textContent = '$' + item.precio.toFixed(2);

        let descripcion = document.createElement('p');
        descripcion.textContent = verifyString(item.desc,32);

        let boton = document.createElement('a');
        boton.classList.add('button');
        boton.href = '#';
        boton.onclick = function () {
            return false
        };
        boton.textContent = 'Comprar';

        boton.addEventListener('click', function () {
            let index = parseInt(item.id, 10);
            if (carrito.indexOf(index) === -1) {
                alert('Se ha añadido al carrito');
                carrito.push(index);
                cantidad.push(1);
                localStorage.setItem("cantidad", JSON.stringify(cantidad));
                localStorage.setItem("carro", JSON.stringify(carrito))
            } else {
                alert('Ya se encuentra en el carrito');
            }
        });

        let botonVer = document.createElement('a');
        botonVer.classList.add('button');
        botonVer.href = '#';
        botonVer.onclick = function () {
            return false
        };
        botonVer.textContent = 'Ver producto';

        botonVer.addEventListener('click', function () {
            focusprod = parseInt(item.id, 10);
            localStorage.setItem("focusprod", focusprod);
            window.location = "../html/ver_prod.html";
        });

        //html construido
        div_inter.appendChild(titulo);
        div_inter.appendChild(precio);
        div_inter.appendChild(descripcion);
        div_inter.appendChild(boton);
        div_inter.appendChild(botonVer);

        card.appendChild(imagen);
        card.appendChild(div_inter);

        items.appendChild(card);

        let carrito_actual = JSON.parse(localStorage.getItem('carro'));
        if (!(carrito_actual == null)) {
            carrito = carrito_actual;
        }
    }
}

function obtenerItem(index){
    for(i=0;i<db.length; i++){
        if(index == db[i].id){
            return db[i];
        }
    }
}

function verifyString(sentence,max){
    if (sentence.length <= max){
        return sentence;
    }else{
        var sub = sentence.substr(0,max);
        sub +="...";
        return sub;
    }
}