var x = document.getElementById("caja");
var y = document.getElementById("caja1");
var z = document.getElementById("registro");
var o = document.getElementById("btn");
var btnRegistrar = document.getElementById("registrar");
var btnLogin = document.getElementById("login1")


var users = localStorage.getItem("users");
users = JSON.parse(users);
if (users === null)
    users = [];

function registro() {
    x.style.left = "-400px";
    z.style.left = "60px";
    y.style.left = "0px";
    o.style.left = "120px";


    x.style.display = "none";
    y.style.display = "block";
}

function login() {
    x.style.left = "0px";
    y.style.left = "450px";
    o.style.left = "0";

    x.style.display = "block";
    y.style.display = "none";
}

function Mensaje(t) {
    switch (t) {
        case 1:
            var newNode = document.createElement('div');
            newNode.className = 'alert-success';
            newNode.innerHTML = 'Gracias';
            document.getElementById('mes').appendChild(newNode);
            setTimeout(function () {
                window.location.reload(1);
            }, 1000);
            break;
        case 2:
            var newNode = document.createElement('div');
            newNode.className = 'alert-danger';
            newNode.innerHTML = 'No Coincide';
            document.getElementById('mes').appendChild(newNode);
            setTimeout(function () {
                window.location.reload(1);
            }, 1000);
            break;
        default:
    }
}

function AgregarUsuario() {
    var alerta = document.getElementById("mes")
    var rol = document.getElementById("Rol").value;
    var user = document.getElementById("user").value;
    var nombre = document.getElementById("nombre").value;
    var apellido = document.getElementById("apellido").value;
    var email = document.getElementById("email").value;
    var direccion = document.getElementById("direccion").value;
    var codigopostal = document.getElementById("codigopostal").value;
    var pais = document.getElementById("pais").value;
    var telefono = document.getElementById("telefono").value;
    var tarjeta = document.getElementById("tarjeta").value;
    var pass = document.getElementById("passs").value;
    var carrito = [];

    
    
    var datos_cliente = {
        rol: rol,
        username: user,
        pass: pass,
        carrito: carrito,
        nombres: nombre,
        apellidos: apellido,
        email: email,
        pais: pais,
        cpostal: codigopostal,
        direccion: direccion,
        tarjeta: tarjeta,
        tel: telefono
    };
    users.push(datos_cliente);
    localStorage.setItem("users", JSON.stringify(users));
}

function logear() {
    var users = JSON.parse(localStorage.getItem("users"));
    var username = document.getElementById("usrnm");
    var pass = document.getElementById("pass");
    var btnLogin = document.getElementById("login");

    for (var i = 0; i < users.length; i++) {
        //console.log(i);
        if (username.value == users[i].username && pass.value == users[i].pass) {
            var globalUser = [{
                id_user: i
            }];
            localStorage.setItem("guser", JSON.stringify(globalUser));
            window.location = "../index.html";

            var carroinvitado = JSON.parse(localStorage.getItem('carro'));
            if (carroinvitado === null) carroinvitado = [];
            var carrouser = users[i].carrito;
            var mergecarro = arrayUnique(carrouser.concat(carroinvitado));
            users[i].carrito = mergecarro;
            localStorage.setItem("users", JSON.stringify(users));

            localStorage.setItem("carro", JSON.stringify(users[i].carrito));

            let cactual = users[i].carrito;
            let cantidad = [];
            for (let i in cactual) {
                cantidad.push(1);
            }
            localStorage.setItem("cantidad", JSON.stringify(cantidad));
            Mensaje(1);
        } else {
            Mensaje(2);
        }
    }
}

function arrayUnique(array) {
    var a = array.concat();
    for (var i = 0; i < a.length; ++i) {
        for (var j = i + 1; j < a.length; ++j) {
            if (a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
}
var datosCorrectos = true;

function validacion() {
    var rol = document.getElementById("Rol").value;
    var user = document.getElementById("user").value;
    var nombre = document.getElementById("nombre").value;
    var apellido = document.getElementById("apellido").value;
    var email = document.getElementById("email").value;
    var direccion = document.getElementById("direccion").value;
    var codigopostal = document.getElementById("codigopostal").value;
    var pais = document.getElementById("pais").value;
    var telefono = document.getElementById("telefono").value;
    var tarjeta = document.getElementById("tarjeta").value;
    var pass = document.getElementById("passs").value;
    var cpass = document.getElementById("cpasss").value;

    var Vuser = new RegExp('(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,15})$', 'g'); //6 caracteres a 15 incluyendo 1 numero ejemplo edward5
    var Vtodo = new RegExp('^[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])+))*$', 'g'); //Todo tipo de texto sin incluir numeros y simbolos
    var Vtodo1 = new RegExp('^[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])+))*$', 'g'); //Todo tipo de texto sin incluir numeros y simbolos
    var Vtodo2 = new RegExp('^[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])+))*$', 'g'); //Todo tipo de texto sin incluir numeros y simbolos
    var Vemail = new RegExp('^[_A-Za-z0-9-]+(.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(.[A-Za-z0-9-]+)*(.[A-Za-z]{2,4})$', 'g'); //somethingxd@algo.es
    var Vpostal = new RegExp('^[0-9]+$', 'g'); //254
    var VTelefono = new RegExp('^[0-9]{4}[\-]{1}[0-9]{4}$', 'g'); //7895-2562
    var VTarjeta = new RegExp('^(4|5)[0-9]{3}[\-]{1}[0-9]{4}[\-]{1}[0-9]{4}[\-]{1}[0-9]{4}$', 'g'); // Ejemplo 4154968792548316
    var Vcontra = new RegExp('(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,15})$', 'g'); // Ejemplo 11223a     al menos un numero y 6 caracteres

    if (Vuser.test(user)) {
        document.getElementById("user").className = 'noerror';
    } else {
        datosCorrectos = false;
        document.getElementById("user").className = 'error';
        return false;
    }

    if (Vtodo.test(nombre)) {
        document.getElementById("nombre").className = 'noerror';
    } else {
        datosCorrectos = false;
        document.getElementById("nombre").className = 'error';
        return false;
    }

    if (Vtodo1.test(apellido)) {
        document.getElementById("apellido").className = 'noerror';
    } else {
        datosCorrectos = false;
        document.getElementById("apellido").className = 'error';
        return false;
    }

    if (Vemail.test(email)) {

        document.getElementById("email").className = 'noerror';
    } else {
        datosCorrectos = false;
        document.getElementById("email").className = 'error';
        return false;
    }

    if (Vtodo2.test(direccion)) {

        document.getElementById("direccion").className = 'noerror';
    } else {
        datosCorrectos = false;
        document.getElementById("direccion").className = 'error';
        return false;
    }


    if (Vpostal.test(codigopostal)) {
        document.getElementById("codigopostal").className = 'noerror';
    } else {
        datosCorrectos = false;
        document.getElementById("codigopostal").className = 'error';
        return false;
    }

    if (VTelefono.test(telefono)) {
        document.getElementById("telefono").className = 'noerror';
    } else {
        datosCorrectos = false;
        document.getElementById("telefono").className = 'error';
        return false;
    }

    if (VTarjeta.test(tarjeta)) {
        document.getElementById("tarjeta").className = 'noerror';
    } else {
        datosCorrectos = false;
        document.getElementById("tarjeta").className = 'error';
        return false;
    }

    if (Vcontra.test(pass)) {
        document.getElementById("passs").className = 'noerror';
    } else {
        datosCorrectos = false;
        document.getElementById("passs").className = 'error';
        return false;
    }

    if (cpass.match(pass)) {
        document.getElementById("cpasss").className = 'noerror';
    } else {
        datosCorrectos = false;
        document.getElementById("cpasss").className = 'error';
        return false;
    }



    datosCorrectos = true;

}

btnRegistrar.addEventListener("click", function () {
    //localStorage.clear();
    var user = document.getElementById("user").value;
    if (user.length != 0) {
        if ((datosCorrectos === true)) {
            AgregarUsuario();
        }
    } else {

    }
})

btnLogin.addEventListener("click", function () {
    logear();
})
