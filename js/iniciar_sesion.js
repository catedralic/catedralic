var users = JSON.parse(localStorage.getItem("users"));
var username = document.getElementById("usrnm");
var pass = document.getElementById("pass");
var btnLogin = document.getElementById("login");

btnLogin.addEventListener("click", function(){
    for (var i = 0; i<users.length; i++){
        if(username.value == users[i].username && pass.value == users[i].pass){
            var globalUser = [{id_user: i}];
            localStorage.setItem("guser", JSON.stringify(globalUser));
            window.location="../index.html";
        }
    }
})