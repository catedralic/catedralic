var btnRegistrar2 = document.getElementById("registrard");
var aparece = document.getElementById("apar");
var xy;
var users = localStorage.getItem("users");
users = JSON.parse(users);
if (users === null)
    users = [];


var guser = JSON.parse(localStorage.getItem("guser"));
var btnVerUsers = document.getElementById("users");
var btnVerInv = document.getElementById("inv");
var tablediv = document.getElementById("tablainfo");


var btnBack = document.getElementById("back");
btnBack.addEventListener("click", function () {
    window.location = "../index.html";
})

if (guser.length <= 0) { //No se ha iniciado sesión
    alert("No tienes permiso para eso...");
    window.location = "../index.html";
} else { //Sí se ha iniciado sesión
}



function editItem(dataId){
        console.log(dataId);
        var user = document.getElementById("user");
        var nombre = document.getElementById("nombre");
        var apellido = document.getElementById("apellido");
        var email = document.getElementById("email");
        var direccion = document.getElementById("direccion");
        var codigopostal = document.getElementById("codigopostal");
        var pais = document.getElementById("pais");
        var telefono = document.getElementById("telefono");
        var tarjeta = document.getElementById("tarjeta");
        var pass = document.getElementById("passs");

        var todoList = [];
        todoList = JSON.parse(localStorage.getItem("users"));
    
        user.value = todoList[dataId].username;
        nombre.value = todoList[dataId].nombre;
        apellido.value = todoList[dataId].apellido;
        email.value = todoList[dataId].correo;
        direccion.value = todoList[dataId].direccion;
        codigopostal.value = todoList[dataId].cpostal;
        telefono.value = todoList[dataId].telefono;
        tarjeta.value = todoList[dataId].tarjeta;
        pass.value = todoList[dataId].pass;
        document.getElementById("registrard").setAttribute("id",dataId);
        document.getElementById(dataId).setAttribute("name","update");
        document.getElementById(dataId).innerText = "Guardar";
        xy = document.getElementsByTagName("button")[0].getAttribute("id"); 
    }


btnVerUsers.addEventListener("click", function () {
    verUsuarios();
    document.getElementById("formProducto1").style.display = "block"; 
})


function verUsuarios(){
    tablediv.innerHTML = "";
    var tbl = document.createElement('table');
    tbl.className = "tb";
    var tbdy = document.createElement('tbody');
    var frow = ["Rol", "User", "Pass", "Carrito", "Nombres", "Apellidos", "Email", "País", "Postal", "Dirección", "Tarjeta", "Tel","Modificar"];
    var tr = document.createElement('tr');
    tr.className = "head";
    for (var j = 0; j < 13; j++) {
        var td = document.createElement('td');
        td.innerHTML = frow[j];
        tr.appendChild(td);
    }
    tbdy.appendChild(tr);
    for (var i = guser[0].id_user; i <= (guser[0].id_user); i++) {
        var tr = document.createElement('tr');
        for (const prop in users[i]) {
            var td = document.createElement('td');
            td.innerHTML = `${users[i][prop]}`;
            tr.appendChild(td);
        }
            var td = document.createElement('td');
            td.innerHTML = "<a class='button' id='apar' onclick='" + "editItem(" + i + ")" + "' >Modificar</a>";
            tr.appendChild(td);
        tbdy.appendChild(tr);
    }
    tbl.appendChild(tbdy);
    tablediv.appendChild(tbl)
}


function AgregarUsuario() {
    var rol = document.getElementById("Rol").value;
    var user = document.getElementById("user").value;
    var nombre = document.getElementById("nombre").value;
    var apellido = document.getElementById("apellido").value;
    var email = document.getElementById("email").value;
    var direccion = document.getElementById("direccion").value;
    var codigopostal = document.getElementById("codigopostal").value;
    var pais = document.getElementById("pais").value;
    var telefono = document.getElementById("telefono").value;
    var tarjeta = document.getElementById("tarjeta").value;
    var pass = document.getElementById("passs").value;
    var carrito = [];
    todoList = [];
    todoList.push({
        rol: rol,
        username: user,
        nombre: nombre,
        apellido: apellido,
        correo: email,
        direccion: direccion,
        cpostal: codigopostal,
        pais: pais,
        telefono: telefono,
        tarjeta: tarjeta,
        pass: pass,
        carrito: carrito,
    });
    var todoListCopia = todoList.slice();
    users.splice(guser[0].id_user, 1, todoListCopia[0]);
    localStorage.setItem("users", JSON.stringify(users));
}


var datosCorrectos=true;

function validacion(){
    var user = document.getElementById("user").value;
    var nombre = document.getElementById("nombre").value;
    var apellido = document.getElementById("apellido").value;
    var email = document.getElementById("email").value;
    var direccion = document.getElementById("direccion").value;
    var codigopostal = document.getElementById("codigopostal").value;
    var pais = document.getElementById("pais").value;
    var telefono = document.getElementById("telefono").value;
    var tarjeta = document.getElementById("tarjeta").value;
    var pass = document.getElementById("passs").value;
    var cpass = document.getElementById("cpasss").value;

    var Vuser = new RegExp('(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,15})$','g');//6 caracteres a 15 incluyendo 1 numero ejemplo edward5
    var Vtodo = new RegExp('^[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])+))*$','g');//Todo tipo de texto sin incluir numeros y simbolos
    var Vtodo1 = new RegExp('^[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])+))*$','g');//Todo tipo de texto sin incluir numeros y simbolos
    var Vtodo2 = new RegExp('^[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])+))*$','g');//Todo tipo de texto sin incluir numeros y simbolos
    var Vemail = new RegExp('[_A-Za-z0-9-]+(.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(.[A-Za-z0-9-]+)*(.[A-Za-z]{2,4})$','g');//somethingxd@algo.es
    var Vpostal = new RegExp('^[0-9]+$','g');//254
    var VTelefono = new RegExp('^[0-9]{4}[\-]{1}[0-9]{4}$','g');//7895-2562
    var VTarjeta = new RegExp('^(4|5)[0-9]{3}[\-]{1}[0-9]{4}[\-]{1}[0-9]{4}[\-]{1}[0-9]{4}$','g');// Ejemplo 4154968792548316
    var Vcontra = new RegExp('(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,15})$','g');// Ejemplo 11223a     al menos un numero y 6 caracteres

    if(Vuser.test(user)){
        document.getElementById("user").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("user").className = 'error';
        return false;
    }

    if(Vtodo.test(nombre)){
        document.getElementById("nombre").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("nombre").className = 'error';
        return false;
    }

    if(Vtodo1.test(apellido)){
        document.getElementById("apellido").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("apellido").className = 'error';
        return false;
    }

    if(Vemail.test(email)){
        
        document.getElementById("email").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("email").className = 'error';
        return false;
    }

    if(Vtodo2.test(direccion)){
        
        document.getElementById("direccion").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("direccion").className = 'error';
        return false;
    }

    
    if(Vpostal.test(codigopostal)){
        document.getElementById("codigopostal").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("codigopostal").className = 'error';
        return false;
    }

    if(VTelefono.test(telefono)){
        document.getElementById("telefono").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("telefono").className = 'error';
        return false;
    }

    if(VTarjeta.test(tarjeta)){
        document.getElementById("tarjeta").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("tarjeta").className = 'error';
        return false;
    }

    if(Vcontra.test(pass)){
        document.getElementById("passs").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("passs").className = 'error';
        return false;
    }

    if(cpass.match(pass)){
        document.getElementById("cpasss").className = 'noerror';
    }
    else{
        datosCorrectos=false;
        document.getElementById("cpasss").className = 'error';
        return false;
    }
    
    datosCorrectos = true;

}

btnRegistrar2.addEventListener("click", function () {
    var user = document.getElementById("user").value;
    if (user.length != 0){
        if((datosCorrectos === true)){
            AgregarUsuario();
        }
    }
    else{

    }
})