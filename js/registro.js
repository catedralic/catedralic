var users = localStorage.getItem("users");
var operacion = "A";
users = JSON.parse(users);
if (users === null)
    users = [];

$('.toggle').click(function(){
    $('.formulario').animate({
        height: "toggle",
        'padding-top': 'toggle',
        'padding-bottom': 'toggle',
        opacity: 'toggle'
    }, "slow");
});

function Mensaje(t){
        switch (t) {
            case 1:
                $(".mensaje-alerta").append(
                    "<div class='alert-success'>Se agrego con exito</div>"
                );
                setTimeout(function(){
                     window.location.reload(1);
                }, 1000);
                break;
            case 2: 
                $(".mensaje-alerta").append(
                    "<div class='alert-danger'>Se elimino el usuario</div>"
                );
                setTimeout(function(){
                     window.location.reload(1);
                }, 1000);
                break;
            default:
        }
    }

function AgregarUsuario() {
    var datos_cliente = JSON.stringify({
        Rol: $("#Rol").val(),
        User: $("#user").val(),
        Nombre : $("#nombre").val(),
        Apellido: $("#apellido").val(),
        Correo : $("#correo").val(),
        Direccion : $("#direccion").val(),
        Cpostal : $("#codigopostal").val(),
        Pais: $("#pais").val(),
        Telefono : $("#telefono").val(),
        Tarjeta : $("#tarjeta").val(),
        Pass: $("#pass").val(),
    });

    users.push(datos_cliente);
    localStorage.setItem("users", JSON.stringify(users));
    ListarUsuarios();
    return Mensaje(1);
}



function ListarUsuarios (){
    $("#user-list").html(
            "<thead>" +
                "<tr>" +
                    "<th> ID </th>" +
                    "<th> Rol </th>" +
                    "<th> Usuario </th>" +
                    "<th> Contraseña </th>" +
                    "<th> Nombre </th>" +
                    "<th> Apellidos </th>" +
                    "<th> E-mail </th>" +
                    "<th> Pais </th>" +
                    "<th> Postal </th>" +
                    "<th> Direccion </th>" +
                    "<th> Tarjeta </th>" +
                    "<th> Telefono </th>" +
                "</tr>" +
            "</thead>" +
            "<tbody>" +
            "</tbody>"
    );
    

    for (var i in users) {
        var d = JSON.parse(users[i]);
        $("#user-list").append(
                        "<tr>" +
                            "<td>" + i + "</td>" +
                            "<td>" + d.Rol + "</td>" +
                            "<td>" + d.User + "</td>" +
                            "<td>" + d.Pass + "</td>" +
                            "<td>" + d.Nombre + "</td>" +
                            "<td>" + d.Apellido + "</td>" +
                            "<td>" + d.Correo + "</td>" +
                            "<td>" + d.Pais + "</td>" +
                            "<td>" + d.Cpostal + "</td>" +
                            "<td>" + d.Direccion + "</td>" +
                            "<td>" + d.Tarjeta + "</td>" +
                            "<td>" + d.Telefono + "</td>" +
                            "<td> <a id='" + i + "' class='btnEliminar'><img src='../img/error.png' width='20px'> </a> </td>" +
                        "</tr>"
                           );
    }
}

if (users.length !== 0) {
    ListarUsuarios();
} else {
    $("#user-list").append("<h2> No hay clientes registrados </h2>");
}

function Eliminar(e){
    users.splice(e, 1);
    localStorage.setItem("users", JSON.stringify(users));
    return Mensaje(2);
}

$(".btnEliminar").bind("click", function(){
    alert("¿ Me quieres eliminar ?");
    indice_selecionado = $(this).attr("id");
    console.log(indice_selecionado);
    console.log(this);
    Eliminar(indice_selecionado);
    ListarUsuarios();
});


$("#user-form").bind("submit", function() {
    debugger;
    if (operacion == "A")
        return AgregarUsuario();
    else {
    }
});

var username = document.getElementById("usrnm");
var pass = document.getElementById("passs");
var btnLogin = document.getElementById("login");
var storedName = localStorage.getItem('User',JSON.stringify(users));
var storedPw = localStorage.getItem('Pass',JSON.stringify(users));

btnLogin.addEventListener("click", function(){
    for (var i = 0; i<users.length; i++){
        if(username.value == users[i].User && pass.value == users[i].Pass){
            var globalUser = [{id_user: i}];
            localStorage.setItem("guser", JSON.stringify(globalUser));
            window.location="../index.html";
        }
    }
})
