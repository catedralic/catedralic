var bdd = JSON.parse(localStorage.getItem("inventario"));
var users = JSON.parse(localStorage.getItem("users"));
var guser = JSON.parse(localStorage.getItem("guser"));
var cantidad = JSON.parse(localStorage.getItem("cantidad"));
var btnComprar = document.getElementById("pagar");
var envio = document.getElementById("entrega");

function genFac(){
    var i = 0.4;
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1;
    var yyyy = hoy.getFullYear();
    hoy = dd+'/'+mm+'/'+yyyy;
    var car = [];
    var carrito = JSON.parse(localStorage.getItem("carro"));
    console.log(carrito);
    var k = 0;
    do{
        for(var j = 0;j<bdd.length;j++){
            if(carrito[k] == bdd[j].id){
                car.push(bdd[j]);
                k++;
            }
        }
    }while(k<carrito.length);
    console.log(car);
    var cant = cantidad;
    console.log(cant);
    var sal = 0;
    var head = [
        "BOULEVARD IBIZA",
        "KM 1 1/2 CALLE A PLAN DEL PINO, SOYAPANGO",
        hoy
    ];
    var doc = new jsPDF({
        orientation: 'p',
        unit: 'mm',
        format: [head.length*25 + car.length*60,100]
        });
    doc.setFontSize(6);
    doc.text(1, i*10, '############################');
    i+=0.5;
    doc.setFontSize(4);
    for(var j=0;j<head.length;j++){
        doc.text(head[j], 18, i*10, 'center');
        i+=0.5;
    }
    doc.setFontSize(6);
    doc.text(1,i*10,'###########################');
    doc.setFontSize(6);
    i+=0.5;
    for(var j=0;j<car.length;j++){
        var c = false;
        if(car[j].nombre.length<15){
            doc.text(1,i*10,car[j].nombre);
        } else {
            c = true;
            var t = car[j].nombre.slice(0,15);
            console.log(t);
            doc.text(1,i*10,t);
            i+=0.5;
            t = car[j].nombre.slice(15,(car[j].nombre).length);
            console.log(t);
            doc.text(1,i*10,t);
            i-=0.5;
        }
        doc.text(23,i*10,"$"+car[j].precio.toString() +"x");
        doc.text(32,i*10,cant[j].toString());
        sal+=(car[j].precio*cant[j]);
        i+=0.5;
        if(c){
            i+=0.5;
        }
    }
    doc.setFontSize(6);
    doc.text(1,i*10,'---------------------------------------------');
    i+=0.5;
    doc.text(1,i*10,"TOTAL A PAGAR");
    doc.text("$"+sal.toString(), 28, i*10, 'center');
    doc.save('web.pdf');
}


btnComprar.addEventListener("click", function () {
    //Verificando si se ha iniciado sesión
    if (guser.length <= 0) { //No se ha iniciado sesión
        alert("Inicia sesión para comprar");
        window.location = "iniciar_sesion.html";
    } else { //Sí se ha iniciado sesión
        if (carrito.length <= 0) { //No tiene nada en el carrito
            alert("No se ha añadido nada al carrito");
        } else {
            alert("Compra realizada con éxito");
            //PROCESO QUE ELIMINA LOS DATOS DEL CARRITO
            genFac();
            //LA FACTURA SE DEBE HACER ANTES DE ESTE PUNTO
            var vacio = [];
            localStorage.setItem("cantidad", JSON.stringify(vacio));
            localStorage.setItem('carro', JSON.stringify(vacio));
            users[guser[0].id_user].carrito = vacio;
            localStorage.setItem('users', JSON.stringify(users));
            location.reload();
        }
    }
})
if (localStorage.getItem("carro") === null) {
    var carrito = [];
    localStorage.setItem("carro", JSON.stringify(carrito));
} else {
    var carrito = JSON.parse(localStorage.getItem("carro"));
}

var indice;
var aux = 0;
var aux2 = 0;

function actualizarTotal() {
    let subtotal_span = document.getElementById('subtotal');
    let total_span = document.getElementById('total');
    let total = 0.0;
    let subtotal = 0.0;
    let env = 0.0;
    let pagar_producto = document.getElementsByClassName('total_pagar');
    for (let i in pagar_producto) {
        if (pagar_producto[i].textContent != undefined) {
            total += parseFloat(pagar_producto[i].textContent.substr(1));
        }
    }
    subtotal = total;
    if (guser.length > 0) { //No se ha iniciado sesión
        switch (users[guser[0].id_user].pais) {
            case "SV":
                envio.innerHTML = "$0.00";
                env = 0.0;
                break;
            case "AG":
            case "AR":
            case "BT":
            case "BB":
            case "BZ":
            case "BO":
            case "BR":
            case "CA":
            case "CL":
            case "CO":
            case "CR":
            case "CU":
            case "DM":
            case "EC":
            case "US":
            case "GD":
            case "GT":
            case "GF":
            case "HT":
            case "HN":
            case "JM":
            case "MX":
            case "NI":
            case "PA":
            case "PY":
            case "PE":
            case "DO":
            case "KN":
            case "VC":
            case "LC":
            case "SR":
            case "TT":
            case "UY":
            case "VE":
                env = total * 0.1;
                total *= 1.1;

                break;
            default:
                env = total * 0.2;
                total *= 1.2;
                break;
        }
    } else {
        envio.innerHTML = "$0.00";
    }

    total_span.textContent = '$' + total.toFixed(2);
    envio.textContent = '$' + env.toFixed(2);
    subtotal_span.textContent = '$' + subtotal.toFixed(2);
}

var items = document.querySelector('#cuerpo');
window.onload = cargardatos;

function cargardatos() {
    for (let i in carrito) {
        let index = carrito[i];
        let item = obtenerItem(index);
        let nodo = document.createElement('tr'); //fila del producto
        nodo.classList.add('text-center');

        //imagen
        let celda_img = document.createElement('td');
        celda_img.classList.add('image-prod');

        let div_img = document.createElement('div');
        div_img.classList.add('img');

        let imagen = document.createElement('img');
        imagen.src = "../" + item.img;
        imagen.alt = "producto";
        //agregar imagen
        div_img.appendChild(imagen);
        celda_img.appendChild(div_img);
        nodo.appendChild(celda_img);

        //info producto
        let celda_nombre = document.createElement('td');
        celda_nombre.classList.add('product-name');
        let nombre = document.createElement('h3');
        nombre.textContent = item.nombre;
        let descripcion = document.createElement('p');
        descripcion.textContent = item.desc;
        celda_nombre.appendChild(nombre);
        celda_nombre.appendChild(descripcion);

        //precio
        let celda_precio = document.createElement('td');
        celda_precio.classList.add('price');
        let precio = item.precio
        let precio_p = document.createElement('p');
        precio_p.textContent = '$' + precio.toFixed(2);
        celda_precio.appendChild(precio_p);

        //total
        let celda_total = document.createElement('td');
        celda_total.classList.add('total');
        let total_producto = document.createElement('p');
        total_producto.classList.add("total_pagar");
        total_producto.textContent = '$' + precio.toFixed(2);
        celda_total.appendChild(total_producto);

        //cantidad
        let celda_cantidad = document.createElement('td');
        celda_cantidad.classList.add('quantity');
        let div_input = document.createElement('div');
        div_input.classList.add('input-group', 'mb-3');
        let input_cantidad = document.createElement('input');
        input_cantidad.classList.add('quantity', 'form-control', 'input-number');
        input_cantidad.setAttribute("type", "number");
        input_cantidad.value = cantidad[i];
        let pagar = input_cantidad.value * precio;
        total_producto.textContent = '$' + pagar.toFixed(2);
        input_cantidad.min = 1;
        input_cantidad.max = 100;
        input_cantidad.addEventListener('input', () => {
            if (input_cantidad.value < 1 || input_cantidad.value > 100) {
                input_cantidad.value = 1;
            }
            if (input_cantidad.value % 1 != 0){
                input_cantidad.value = 1;
            }
            let pagar = input_cantidad.value * precio;
            total_producto.textContent = '$' + pagar.toFixed(2);
            cantidad[i] = parseInt(input_cantidad.value);
            localStorage.setItem("cantidad", JSON.stringify(cantidad));
            actualizarTotal();
        });
        input_cantidad.addEventListener('focus', () => {
            input_cantidad.select();
        });
        div_input.appendChild(input_cantidad);
        celda_cantidad.appendChild(div_input);

        //borrar
        let celda_borrar = document.createElement('td');
        let boton_borrar = document.createElement('a');
        boton_borrar.href = '#';
        boton_borrar.onclick = () => {
            return false
        };
        boton_borrar.addEventListener('click', () => {
            indice = i;
            items.removeChild(nodo);
            if (indice < aux2) {
                carrito.splice(indice, 1);
                if (guser.length > 0) { //Sí se ha iniciado sesión
                    cantidad.splice(indice, 1);
                    localStorage.setItem('cantidad', JSON.stringify(cantidad));
                }
            } else {
                carrito.splice(indice - aux, 1);
                if (guser.length > 0) { //Sí se ha iniciado sesión
                    cantidad.splice(indice - aux, 1);
                    localStorage.setItem('cantidad', JSON.stringify(cantidad));
                }
                aux++;
            }
            //console.log(indice - aux);
            //console.log(indice - aux);
            aux2 = indice - aux;

            if (guser.length > 0) { //Sí se ha iniciado sesión
                localStorage.setItem('carro', JSON.stringify(carrito));
                users[guser[0].id_user].carrito = carrito;
                localStorage.setItem('users', JSON.stringify(users));
            }

            actualizarTotal();
        });
        let imagen_borrar = document.createElement('img');
        imagen_borrar.src = "../img/remove.png";
        imagen_borrar.alt = "borrar";
        imagen_borrar.classList.add("btn-borrar");
        boton_borrar.appendChild(imagen_borrar);
        celda_borrar.appendChild(boton_borrar);

        nodo.appendChild(celda_img);
        nodo.appendChild(celda_nombre);
        nodo.appendChild(celda_precio);
        nodo.appendChild(celda_cantidad);
        nodo.appendChild(celda_total);
        nodo.appendChild(celda_borrar);

        items.appendChild(nodo);
    }
    actualizarTotal();
}

function obtenerItem(index) {
    for (i = 0; i < bdd.length; i++) {
        if (index == bdd[i].id) {
            return bdd[i];
        }
    }
}
