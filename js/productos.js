var db = [{
    id: 0,
    nombre: "Sweatshirt Women Letter Crop Top",
    desc: "Girl harajuku streetwear",
    img: "img/girl.jpg",
    precio: 23.95
}, {
    id: 1,
    nombre: "Coffee Heart Pulse Black Hoodies",
    desc: "We love coffee",
    img: "img/coffee.jpg",
    precio: 30.50
}, {
    id: 2,
    nombre: "Casual loose sweatpants",
    desc: "Khaki women pants",
    img: "img/joggers.jpg",
    precio: 17.99
}, {
    id: 3,
    nombre: "Casual pants",
    desc: "Casual pants for women",
    img: "img/casual.jpg",
    precio: 15.99
}, {
    id: 4,
    nombre: "Nike sportswear pants",
    desc: "Nike original",
    img: "img/nike.jpg",
    precio: 95.95
}, {
    id: 5,
    nombre: "Male Hip Hop Long Sleeve",
    desc: "Long sleeve version",
    img: "img/hiphop.jpg",
    precio: 22.0
}, {
    id: 6,
    nombre: "Bruce Lee T-Shirt",
    desc: "Brule Lee original T-Shirt",
    img: "img/lee.jpg",
    precio: 59.99
}, {
    id: 7,
    nombre: "Crossfit summer T-Shirt",
    desc: "A fitness crossfit T-Shirt",
    img: "img/crossfit.jpg",
    precio: 16.10
}, {
    id: 8,
    nombre: "Multi-pocket Pant Elastic Design",
    desc: "Hip Hop Harem Style!",
    img: "img/multipocket.jpg",
    precio: 69.99
}, {
    id: 9,
    nombre: "Elastic waist jogger pants",
    desc: "28% OFF!!!",
    img: "img/elastic.jpg",
    precio: 22.59
}, {
    id: 10,
    nombre: "Baseball cap casual summer",
    desc: "#PERCE",
    img: "img/perce.jpg",
    precio: 13.35
}, {
    id: 11,
    nombre: "Print graffiti cap",
    desc: "Adjustable snapback",
    img: "img/graffiti.jpg",
    precio: 18.99
}, {
    id: 12,
    nombre: "Streetwear chain necklace",
    desc: "Gold version",
    img: "img/necklace.jpeg",
    precio: 60.00
}, {
    id: 13,
    nombre: "Chain with lock",
    desc: "A chain nacklace with lock",
    img: "img/lock.jpg",
    precio: 90.99
}, {
    id: 14,
    nombre: "SUPREME gloves",
    desc: "SUPREME EXCLUSIVE",
    img: "img/supreme.jpg",
    precio: 199.99
}]


var carrito = [];
if (localStorage.getItem("rol") === null) {
    var rol = [{
        nombre: "admin"
    }, {
        nombre: "user"
    }]
    localStorage.setItem("rol", JSON.stringify(rol));
} else {
    var rol = JSON.parse(localStorage.getItem("rol"));
}

if (localStorage.getItem("users") === null) {
    var users = [{
        rol: rol[0].nombre,
        username: "admin",
        pass: "admin",
        carrito: [],
        nombres: "Benito Juarez",
        apellidos: "Camelo Rodriguez",
        email: "camelo.benito@cdb.edu.sv",
        pais: "SV",
        cpostal: 1116,
        direccion: "Km 1 1/2 Calle a plan del Pino, Soyapango",
        tarjeta: "1111-1111-1111-326",
        tel: "2121-2828"
    }]
    localStorage.setItem("users", JSON.stringify(users));
} else {
    var users = JSON.parse(localStorage.getItem("users"));
}

if (localStorage.getItem("inventario") === null) {
    localStorage.setItem("inventario", JSON.stringify(db));
    var adb = shuffle(db.slice());
} else {
    var cdb = JSON.parse(localStorage.getItem("inventario"));
    var adb = shuffle(cdb.slice());
}


var btnlogin = document.getElementById("login");
//localStorage.removeItem("users");

if (localStorage.getItem("guser") === null) {
    var globalUser = [];
    localStorage.setItem("guser", JSON.stringify(globalUser));
    var guser = JSON.parse(localStorage.getItem("guser"));
} else {
    var guser = JSON.parse(localStorage.getItem("guser"));
}

if (localStorage.getItem("cantidad") === null) {
    var cantidad = [];
    localStorage.setItem("cantidad", JSON.stringify(cantidad));
    var cantidad = JSON.parse(localStorage.getItem("cantidad"));
} else {
    var cantidad = JSON.parse(localStorage.getItem("cantidad"));
}

if (localStorage.getItem("focusprod") === null) {
    localStorage.setItem("focusprod", 0);
    var focusprod = localStorage.getItem("focusprod");
} else {
    var focusprod = localStorage.getItem("focusprod");
}

var items = document.querySelector('#principal');

window.onload = renderItems;

var botones;

function renderItems() {
    loadSlider();
    //Verificando si se ha iniciado sesión
    if (guser.length <= 0) { //No se ha iniciado sesión
        btnlogin.innerHTML = "<a href='#'>Iniciar sesión/Registrarse</a>"
        btnlogin.addEventListener("click", function () {
            window.location = "html/iniciar_sesion.html";
        })
        let panelU = document.getElementById("panelU");
            panelU.style.visibility = "hidden";
        let panel = document.getElementById("admin");
        panel.style.visibility = "hidden";
    } else { //Sí se ha iniciado sesión
        
        btnlogin.innerHTML = "<a href='#'>Cerrar sesión</a>"
        btnlogin.addEventListener("click", function () {
            users[guser[0].id_user].carrito = JSON.parse(localStorage.getItem('carro'));
            localStorage.setItem("users", JSON.stringify(users));
            localStorage.setItem("carro", JSON.stringify(new Array()));
            localStorage.removeItem("guser");
            var vacio = [];
            localStorage.setItem("cantidad", JSON.stringify(vacio));
            location.reload();
        })
        
        let panelU = document.getElementById("panelU");
            panelU.style.visibility = "visible";
        
        let panel = document.getElementById("admin");
        if (users[guser[0].id_user].rol == "admin") {
            panel.style.visibility = "visible";
            console.log("Hola " + users[guser[0].id_user].username);
        }else{
            panel.style.visibility = "hidden";   
        }
    }

    for (let i in adb) {
        //estructura
        let card = document.createElement('div');
        card.classList.add('card');

        let imagen = document.createElement('img');
        imagen.src = adb[i].img;

        //estructura interna
        let div_inter = document.createElement('div');

        let titulo = document.createElement('h4');
        titulo.textContent = verifyString(adb[i].nombre,32);

        let precio = document.createElement('h5');
        precio.textContent = '$' + adb[i].precio.toFixed(2);

        let descripcion = document.createElement('p');
        descripcion.textContent = verifyString(adb[i].desc,32);

        let boton = document.createElement('a');
        boton.classList.add('button');
        boton.href = '#';
        boton.onclick = function () {
            return false
        };
        boton.textContent = 'Comprar';

        boton.addEventListener('click', function () {
            let index = parseInt(adb[i].id, 10);
            if (carrito.indexOf(index) === -1) {
                alert('Se ha añadido al carrito');
                carrito.push(index);
                cantidad.push(1);
                localStorage.setItem("carro", JSON.stringify(carrito));
                localStorage.setItem("cantidad", JSON.stringify(cantidad));
            } else {
                alert('Ya se encuentra en el carrito');
            }
        });

        let botonVer = document.createElement('a');
        botonVer.classList.add('button');
        botonVer.href = '#';
        botonVer.onclick = function () {
            return false
        };
        botonVer.textContent = 'Ver producto';

        botonVer.addEventListener('click', function () {
            focusprod = parseInt(adb[i].id, 10);
            localStorage.setItem("focusprod", focusprod);
            window.location = "html/ver_prod.html";
        });

        //html construido
        div_inter.appendChild(titulo);
        div_inter.appendChild(precio);
        div_inter.appendChild(descripcion);
        div_inter.appendChild(boton);
        div_inter.appendChild(botonVer);

        card.appendChild(imagen);
        card.appendChild(div_inter);

        items.appendChild(card);

        let carrito_actual = JSON.parse(localStorage.getItem('carro'));
        if (!(carrito_actual == null)) {
            carrito = carrito_actual;
        }
    }
}

function loadSlider(){
    var l = document.anchors;
    l[0].addEventListener("click", function(){
        var indices = new Array();
        for (var i = 0;i<5;i++){
            indices.push(i);
        }
        localStorage.setItem("promo", JSON.stringify(indices));
        window.location = "html/promo.html";
    })
    
    l[1].addEventListener("click", function(){
        var indices = new Array();
        for (var i = 5;i<10;i++){
            indices.push(i);
        }
        localStorage.setItem("promo", JSON.stringify(indices));
        window.location = "html/promo.html";
    })
    
    l[2].addEventListener("click", function(){
        var indices = new Array();
        for (var i = 10;i<15;i++){
            indices.push(i);
        }
        localStorage.setItem("promo", JSON.stringify(indices));
        window.location = "html/promo.html";
    })
}

function verifyString(sentence,max){
    if (sentence.length <= max){
        return sentence;
    }else{
        var sub = sentence.substr(0,max);
        sub +="...";
        return sub;
    }
}

function shuffle(array) {
    var m = array.length,
        t, i;

    // While there remain elements to shuffle...
    while (m) {

        // Pick a remaining element…
        i = Math.floor(Math.random() * m--);

        // And swap it with the current element.
        t = array[m];
        array[m] = array[i];
        array[i] = t;
    }
    return array;
}
